import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { OnModuleInit } from '@nestjs/common';
import { Socket } from 'socket.io';

type Point = {
  x: number;
  y: number;
};

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class AppGateway
  implements OnModuleInit, OnGatewayConnection, OnGatewayDisconnect
{
  onModuleInit(): void {
    //
  }

  handleConnection(@ConnectedSocket() socket: Socket): void {
    console.log(`A player has connected${socket}`);
  }

  handleDisconnect(@ConnectedSocket() socket: Socket): void {
    console.log(`A player has disconnected${socket}`);
  }

  @SubscribeMessage('FIRST_POINT_FROM_PLAYER')
  start(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
    console.log('FIRST_POINT_FROM_PLAYER');
    socket.broadcast.emit('FIRST_POINT_TO_PLAYERS', point);
  }

  @SubscribeMessage('POINT_FROM_PLAYER')
  addPoint(
    @ConnectedSocket() socket: Socket,
    @MessageBody() point: Point,
  ): void {
    console.log('POINT_FROM_PLAYER');
    socket.broadcast.emit('POINT_TO_PLAYERS', point);
  }
}
