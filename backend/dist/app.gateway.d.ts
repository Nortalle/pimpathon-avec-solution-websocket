import { OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { OnModuleInit } from '@nestjs/common';
import { Socket } from 'socket.io';
type Point = {
    x: number;
    y: number;
};
export declare class AppGateway implements OnModuleInit, OnGatewayConnection, OnGatewayDisconnect {
    onModuleInit(): void;
    handleConnection(socket: Socket): void;
    handleDisconnect(socket: Socket): void;
    start(socket: Socket, point: Point): void;
    addPoint(socket: Socket, point: Point): void;
}
export {};
