import { LineProvider } from "@/components/lineProvider";
import { WebSocketProvider } from "@/components/webSocketProvider";
import dynamic from "next/dynamic";

const Sketch = dynamic(
  () => import("../components/sketch").then((mod) => mod.Sketch),
  {
    ssr: false,
  }
);

export default function Home() {
  return (
    <>
      <LineProvider>
        <WebSocketProvider>
          <Sketch />
        </WebSocketProvider>
      </LineProvider>
    </>
  );
}
