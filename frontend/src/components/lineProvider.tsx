import * as React from "react";

type Point = { x: number; y: number };
type LineData = number[];
type ActionName = "ADD_POINT" | "ADD_FIRST_POINT";
type Action = { type: ActionName; point: Point };
type Dispatch = (action: Action) => void;
type State = LineData[];
type LineProviderProps = { children: React.ReactNode };

const LineStateContext = React.createContext<
  { state: State; dispatch: Dispatch } | undefined
>(undefined);

function lineReducer(state: State, action: Action): State {
  const { type, point } = action;
  switch (type) {
    case "ADD_FIRST_POINT": {
      return [...state, [point.x, point.y]];
    }
    case "ADD_POINT": {
      const lines = [...state];

      let [lastLine] = lines.splice(-1);
      return [...lines, [...lastLine, point.x, point.y]];
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
}

function LineProvider({ children }: LineProviderProps) {
  const [state, dispatch] = React.useReducer(lineReducer, []);
  const value = { state, dispatch };
  return (
    <LineStateContext.Provider value={value}>
      {children}
    </LineStateContext.Provider>
  );
}

function useLines() {
  const context = React.useContext(LineStateContext);
  if (context === undefined) {
    throw new Error("useLines must be used within a LineProvider");
  }
  return context;
}

export { LineProvider, useLines };
