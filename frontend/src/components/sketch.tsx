import React, { useContext } from "react";
import Konva from "konva";
import { Stage, Layer, Line } from "react-konva";
import { useLines } from "./lineProvider";
import { useWebSocket } from "./webSocketProvider";

type Point = {
  x: number;
  y: number;
};

export const Sketch = () => {
  const { dispatch: dispatchLines, state } = useLines();
  const [emitFirstPointFromPlayer, emitPointFromPlayer] = useWebSocket();

  const isDrawing = React.useRef(false);

  const getPointFromMouseEvent = (
    mouseEvent: Konva.KonvaEventObject<MouseEvent>
  ): Point => {
    return mouseEvent.target.getStage()?.getPointerPosition() ?? { x: 0, y: 0 };
  };

  const handleMouseDown = (mouseEvent: Konva.KonvaEventObject<MouseEvent>) => {
    if (!isDrawing.current) {
      isDrawing.current = true;
      const point = getPointFromMouseEvent(mouseEvent);
      dispatchLines({ type: "ADD_FIRST_POINT", point: point });
      emitFirstPointFromPlayer(point);
    }
  };

  const handleMouseMove = (mouseEvent: Konva.KonvaEventObject<MouseEvent>) => {
    // no drawing - skipping
    if (!isDrawing.current) {
      return;
    }
    const point = getPointFromMouseEvent(mouseEvent);
    dispatchLines({ type: "ADD_POINT", point: point });
    emitPointFromPlayer(point);
  };

  const handleMouseUp = () => {
    isDrawing.current = false;
  };

  return (
    <div>
      <Stage
        width={window.innerWidth}
        height={window.innerHeight}
        onMouseDown={handleMouseDown}
        onMousemove={handleMouseMove}
        onMouseup={handleMouseUp}
      >
        <Layer>
          {state.map((line, i) => (
            <Line
              key={i}
              points={line}
              stroke="#df4b26"
              strokeWidth={5}
              tension={0.5}
              lineCap="round"
              lineJoin="round"
            />
          ))}
        </Layer>
      </Stage>
    </div>
  );
};
