import { createContext, useContext, useEffect, useMemo } from "react";
import { useLines } from "./lineProvider";
import { Socket, io } from "socket.io-client";
type Point = {
  x: number;
  y: number;
};
type State = Function[];
type WebSocketProviderProps = { children: React.ReactNode };

const WebSocketContext = createContext<State | undefined>(undefined);

function WebSocketProvider({ children }: WebSocketProviderProps): JSX.Element {
  const socket: Socket = useMemo(
    () => io("localhost:8080", { autoConnect: false }),
    []
  );

  useEffect(() => {
    if (!socket.connected) {
      socket.connect();
    }
    return () => {
      socket.close();
    };
  }, [socket]);

  const { dispatch } = useLines();

  useEffect(() => {
    if (!socket) return;

    socket.on("FIRST_POINT_TO_PLAYERS", async (point: Point) => {
      dispatch({ type: "ADD_FIRST_POINT", point: point });
    });

    socket.on("POINT_TO_PLAYERS", async (point: Point) => {
      dispatch({ type: "ADD_POINT", point: point });
    });

    return () => {
      socket.close();
    };
  }, [socket, dispatch]);

  const emitFirstPointFromPlayer = (point: Point) => {
    socket.emit("FIRST_POINT_FROM_PLAYER", point);
  };

  const emitPointFromPlayer = (point: Point) => {
    socket.emit("POINT_FROM_PLAYER", point);
  };

  const value = [emitFirstPointFromPlayer, emitPointFromPlayer];

  return (
    <WebSocketContext.Provider value={value}>
      {children}
    </WebSocketContext.Provider>
  );
}

function useWebSocket(): State {
  const context = useContext(WebSocketContext);
  if (context === undefined) {
    throw new Error("useWebSocket must be used within a WebSocketProvider");
  }
  return context;
}

export { WebSocketProvider, useWebSocket };
